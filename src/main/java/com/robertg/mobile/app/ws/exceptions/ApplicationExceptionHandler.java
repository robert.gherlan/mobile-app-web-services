package com.robertg.mobile.app.ws.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.robertg.mobile.app.ws.ui.model.response.ErrorMessage;

@ControllerAdvice
public class ApplicationExceptionHandler {

	@ExceptionHandler(value = { UserServiceException.class })
	public ResponseEntity<ErrorMessage> handleUserServiceException(UserServiceException e) {
		return getResponseEntity(e);
	}

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<ErrorMessage> handleException(Exception e) {
		return getResponseEntity(e);
	}

	ResponseEntity<ErrorMessage> getResponseEntity(Exception e) {
		var errorMessage = new ErrorMessage(e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
	}
}
