package com.robertg.mobile.app.ws.ui.model.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum RequestOperationStatus {
	SUCCESS("SUCCESS"), ERROR("ERROR");

	private final String name;

}
