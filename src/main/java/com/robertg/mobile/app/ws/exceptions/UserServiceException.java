package com.robertg.mobile.app.ws.exceptions;

import com.robertg.mobile.app.ws.ui.model.response.ErrorMessages;

public class UserServiceException extends RuntimeException {

	private static final long serialVersionUID = -4687700256560061319L;

	public UserServiceException(String message) {
		super(message);
	}

	public UserServiceException(ErrorMessages errorMessages) {
		this(errorMessages.getMessage());
	}
}
