package com.robertg.mobile.app.ws.service.impl;

import static java.util.Objects.isNull;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.robertg.mobile.app.ws.config.security.UserPrincipal;
import com.robertg.mobile.app.ws.repository.UserRepository;
import com.robertg.mobile.app.ws.ui.model.response.ErrorMessages;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private final UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		var userEntity = userRepository.findByEmail(username);
		if (isNull(userEntity)) {
			throw new UsernameNotFoundException(ErrorMessages.NO_RECORD_FOUND.getMessage());
		}

		
		return new UserPrincipal(userEntity);
		//return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(),
		//		userEntity.isEmailVerificationStatus(), true, true, true, emptyList());
	}
}
