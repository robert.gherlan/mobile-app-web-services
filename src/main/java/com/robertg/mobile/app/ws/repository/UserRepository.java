package com.robertg.mobile.app.ws.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.robertg.mobile.app.ws.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
	UserEntity findByEmail(String email);

	UserEntity findByUserId(String id);

	UserEntity findUserByEmailVerificationToken(String token);

	@Query(value = "SELECT * FROM USERS u WHERE u.email_verification_status = 'true'", countQuery = "SELECT COUNT(*) FROM USERS u WHERE u.email_verification_status = 'true'", nativeQuery = true)
	Page<UserEntity> findAllUsersWithConfirmedEmailAddress(Pageable pageable);

	@Query(value = "SELECT * FROM USERS u WHERE u.first_name = ?1", nativeQuery = true)
	List<UserEntity> findUsersByFirstName(String firstName);

	@Query(value = "SELECT * FROM USERS u WHERE u.last_name = :lastName", nativeQuery = true)
	List<UserEntity> findUsersByLastName(@Param("lastName") String lastName);

	@Query(value = "SELECT * FROM USERS u WHERE u.first_name LIKE %:keyword% OR u.last_name LIKE %:keyword%", nativeQuery = true)
	List<UserEntity> findUsersByKeyword(@Param("keyword") String keyword);

	@Query(value = "SELECT u.first_name, u.last_name FROM USERS u WHERE u.first_name LIKE %:keyword% OR u.last_name LIKE %:keyword%", nativeQuery = true)
	List<Object[]> findUserFirstNameAndLastNameByKeyword(@Param("keyword") String keyword);

	@Transactional
	@Modifying(clearAutomatically = true, flushAutomatically = true)
	@Query(value = "UPDATE USERS u SET u.email_verification_status = :emailVerificationStatus WHERE u.user_id = :userId", nativeQuery = true)
	void updateUserEmailVerificationStatus(@Param("emailVerificationStatus") boolean emailVerificationStatus,
			@Param("userId") String userId);

	@Query("SELECT user FROM UserEntity user WHERE user.userId = :userId")
	UserEntity findUserEntityByUserId(@Param("userId") String userId);

	@Query("SELECT user.firstName, user.lastName FROM UserEntity user WHERE user.userId = :userId")
	List<Object[]> getUserEntityFullNameByUserId(@Param("userId") String userId);

	@Transactional
	@Modifying(clearAutomatically = true, flushAutomatically = true)
	@Query(value = "UPDATE UserEntity user SET user.emailVerificationStatus = :emailVerificationStatus WHERE user.userId = :userId")
	void updateUserEntityEmailVerificationStatus(@Param("emailVerificationStatus") boolean emailVerificationStatus,
			@Param("userId") String userId);
}
