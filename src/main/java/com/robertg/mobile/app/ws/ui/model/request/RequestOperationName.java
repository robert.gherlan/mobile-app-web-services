package com.robertg.mobile.app.ws.ui.model.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum RequestOperationName {
	DELETE("DELETE"), VERIFY_EMAIL("VERIFY_EMAIL"), REQUEST_RESET_PASSWORD("REQUEST_RESET_PASSWORD"),
	RESET_PASSWORD("RESET_PASSWORD");

	private final String name;

}
