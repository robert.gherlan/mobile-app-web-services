package com.robertg.mobile.app.ws.service;

import java.util.List;

import com.robertg.mobile.app.ws.shared.dto.UserDto;

public interface UserService {

	UserDto createUser(UserDto userDto);

	UserDto getUserByEmail(String userName);

	UserDto getUserByUserId(String id);

	UserDto updateUser(String id, UserDto userDto);

	void deleteUser(String userId);

	List<UserDto> getUsers(int page, int limit);

	boolean verifyEmailToken(String token);

	boolean requestPasswordReset(String email);

	boolean resetPassword(String token, String password);
}
