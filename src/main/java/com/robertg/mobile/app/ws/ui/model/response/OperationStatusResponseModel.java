package com.robertg.mobile.app.ws.ui.model.response;

import com.robertg.mobile.app.ws.ui.model.request.RequestOperationName;
import com.robertg.mobile.app.ws.ui.model.request.RequestOperationStatus;

import lombok.Data;

@Data
public class OperationStatusResponseModel {
	private RequestOperationName operationName;
	private RequestOperationStatus operationResult;
}
