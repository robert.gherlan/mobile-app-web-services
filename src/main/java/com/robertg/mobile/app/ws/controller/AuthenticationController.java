package com.robertg.mobile.app.ws.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.robertg.mobile.app.ws.ui.model.request.UserLoginRequestModel;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
public class AuthenticationController {

	@ApiResponses(value = { @ApiResponse(code = 200, message = "Response Headers", responseHeaders = {
			@ResponseHeader(name = "Authorization", description = "Bearer <JWT Token Here>", response = String.class),
			@ResponseHeader(name = "User-ID", description = "<Public User ID value here>", response = String.class) }) })
	@PostMapping("/users/login")
	public void fakeLogin(@RequestBody UserLoginRequestModel userLoginRequestModel) {
		throw new IllegalStateException(
				"This method should not be called. This method is implemented by Spring Security.");
	}
}
