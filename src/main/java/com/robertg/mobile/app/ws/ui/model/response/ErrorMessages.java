package com.robertg.mobile.app.ws.ui.model.response;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ErrorMessages {
	MISSING_REQUIRED_FIELD("Missing required field. Please check documentation for required fields."),
	RECORD_ALREADY_EXISTS("Record already exists."), INTERNAL_SERVER_ERROR("Internal server error"),
	NO_RECORD_FOUND("Record with provided id was not found."), AUTHENTICATION_FAILED("Authentication failed."),
	COULD_NOT_UPDATE_RECORD("Could not update record."), COULD_NOT_DELETE_RECORD("Could not delete record."),
	EMAIL_ADRESS_NOT_VERIFIED("Email address not verified.");

	private final String message;
}
