package com.robertg.mobile.app.ws.config.security;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public final class SecurityConstants {

	public static final long EXPIRATION_TIME = 864_000_000;

	public static final long PASSWORD_RESET_EXPIRATION_TIME = 1000 * 60 * 60;

	public static final String TOKEN_PREFIX = "Bearer ";

	public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

	public static final String USER_ID_HEADER_NAME = "User-ID";

	public static final String SIGN_UP_URL = "/users";

	public static final String LOGIN_URL = "/users/login";

	public static final String EMAIL_VERIFICATION_URL = "/users/email-verification";

	public static final String PASSWORD_RESET_REQUEST_URL = "/users/password-reset-request";

	public static final String PASSWORD_RESET_URL = "/users/password-reset";

	public static final Key PRIVATE_KEY = getPrivateKey();

	public static Key getPrivateKey() {
		try {
			var privateKeyUri = ClassLoader.getSystemResource("security/private_key_pkcs8.pem").toURI();
			var privateKeyPath = Paths.get(privateKeyUri);
			var privateKeyContent = Files.readString(privateKeyPath);

			privateKeyContent = privateKeyContent.replaceAll("[\\r|\\n]", "").replace("-----BEGIN PRIVATE KEY-----", "")
					.replace("-----END PRIVATE KEY-----", "");

			var keyFactory = KeyFactory.getInstance("RSA");

			byte[] decodedPrivateKey = Base64.getDecoder().decode(privateKeyContent);
			var keySpecPKCS8 = new PKCS8EncodedKeySpec(decodedPrivateKey);

			return keyFactory.generatePrivate(keySpecPKCS8);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	private SecurityConstants() {
		throw new IllegalStateException();
	}
}
