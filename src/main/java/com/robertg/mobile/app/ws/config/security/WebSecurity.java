package com.robertg.mobile.app.ws.config.security;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.robertg.mobile.app.ws.config.security.filters.AuthenticationFilter;
import com.robertg.mobile.app.ws.config.security.filters.AuthorizationFilter;
import com.robertg.mobile.app.ws.repository.UserRepository;
import com.robertg.mobile.app.ws.service.UserService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, jsr250Enabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

	private final UserDetailsService userDetailsService;

	private final UserService userService;

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
				.mvcMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL).permitAll()
				.mvcMatchers(HttpMethod.POST, SecurityConstants.PASSWORD_RESET_REQUEST_URL).permitAll()
				.mvcMatchers(HttpMethod.POST, SecurityConstants.PASSWORD_RESET_URL).permitAll()
				.mvcMatchers(HttpMethod.GET, SecurityConstants.EMAIL_VERIFICATION_URL).permitAll()
				.antMatchers("/documentation/**", "/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**")
				.permitAll().antMatchers(HttpMethod.DELETE, "/users/**").hasAuthority("DELETE_AUTHORITY").anyRequest()
				.authenticated().and().addFilter(authenticationFilter()).addFilter(authorizationFilter())
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.headers().frameOptions().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

	protected AuthenticationFilter authenticationFilter() throws Exception {
		var authenticationFilter = new AuthenticationFilter(authenticationManager(), userService);
		authenticationFilter.setFilterProcessesUrl(SecurityConstants.LOGIN_URL);

		return authenticationFilter;
	}

	protected AuthorizationFilter authorizationFilter() throws Exception {
		return new AuthorizationFilter(authenticationManager(), userRepository);
	}

	@Bean
	protected CorsConfigurationSource corsConfigurationSource() {
		var corsConfiguration = new CorsConfiguration();

		corsConfiguration.setAllowedOrigins(List.of("*"));
		corsConfiguration.setAllowedMethods(List.of("*"));
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.setAllowedHeaders(List.of("*"));

		var source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", corsConfiguration);

		return source;
	}
}
