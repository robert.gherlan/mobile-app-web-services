package com.robertg.mobile.app.ws.service;

import com.robertg.mobile.app.ws.shared.dto.UserDto;

public interface EmailService {
	public void verifyEmail(UserDto userDto);

	public boolean sendPasswordResetRequest(String firstName, String email, String token);
}
