package com.robertg.mobile.app.ws.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "roles")
public class RoleEntity implements Serializable {

	private static final long serialVersionUID = 2034323882058632508L;

	@Id
	@GeneratedValue
	private long id;

	@Column(unique = true, length = 50)
	private String name;

	@ManyToMany(mappedBy = "roles")
	private List<UserEntity> users;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinTable(name = "roles_authorities", joinColumns = @JoinColumn(name = "roles_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "authorities_id", referencedColumnName = "id"))
	private List<AuthorityEntity> authorities;

	public RoleEntity(String name, List<AuthorityEntity> authorities) {
		this.name = name;
		this.authorities = authorities;
	}
}
