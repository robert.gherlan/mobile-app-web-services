package com.robertg.mobile.app.ws.config.security;

import static java.util.Collections.unmodifiableCollection;
import static org.springframework.util.CollectionUtils.isEmpty;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.robertg.mobile.app.ws.entity.AuthorityEntity;
import com.robertg.mobile.app.ws.entity.UserEntity;

public class UserPrincipal implements UserDetails {

	private static final long serialVersionUID = -1653609489541286004L;

	private final UserEntity userEntity;

	private final Collection<GrantedAuthority> authorities;

	public UserPrincipal(UserEntity userEntity) {
		this.userEntity = userEntity;
		var extractedAuthorities = getAuthorities(userEntity);
		this.authorities = extractedAuthorities;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return unmodifiableCollection(authorities);
	}

	@Override
	public String getPassword() {
		return userEntity.getEncryptedPassword();
	}

	@Override
	public String getUsername() {
		return userEntity.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return userEntity.isEmailVerificationStatus();
	}

	public String getUserId() {
		return userEntity.getUserId();
	}

	private Collection<GrantedAuthority> getAuthorities(UserEntity userEntity) {
		var extractedAuthorities = new HashSet<GrantedAuthority>();
		var authorityEntities = new HashSet<AuthorityEntity>();
		var roles = userEntity.getRoles();
		if (!isEmpty(roles)) {
			roles.forEach(role -> {
				extractedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
				authorityEntities.addAll(role.getAuthorities());
			});
		}

		authorityEntities
				.forEach(authority -> extractedAuthorities.add(new SimpleGrantedAuthority(authority.getName())));

		return extractedAuthorities;
	}
}
