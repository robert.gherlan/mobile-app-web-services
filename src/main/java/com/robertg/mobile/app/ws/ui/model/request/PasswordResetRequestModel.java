package com.robertg.mobile.app.ws.ui.model.request;

import lombok.Data;

@Data
public class PasswordResetRequestModel {
	private String email;
}
