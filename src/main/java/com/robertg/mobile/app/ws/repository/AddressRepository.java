package com.robertg.mobile.app.ws.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.robertg.mobile.app.ws.entity.AddressEntity;
import com.robertg.mobile.app.ws.entity.UserEntity;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {
	List<AddressEntity> findAllByUserDetails(UserEntity userEntity);

	AddressEntity findByAddressIdAndUserDetails(String addressId, UserEntity userEntity);
}
