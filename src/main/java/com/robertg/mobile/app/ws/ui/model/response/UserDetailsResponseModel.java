package com.robertg.mobile.app.ws.ui.model.response;

import java.util.List;

import lombok.Data;

@Data
public class UserDetailsResponseModel {
	private String userId;
	private String firstName;
	private String lastName;
	private String email;
	private List<AddressResponseModel> addresses;
}
