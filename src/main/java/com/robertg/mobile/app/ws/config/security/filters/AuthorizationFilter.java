package com.robertg.mobile.app.ws.config.security.filters;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.robertg.mobile.app.ws.config.security.SecurityConstants;
import com.robertg.mobile.app.ws.config.security.UserPrincipal;
import com.robertg.mobile.app.ws.repository.UserRepository;

import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {

	private final UserRepository userRepository;

	public AuthorizationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
		super(authenticationManager);
		this.userRepository = userRepository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		var authorizationHeader = request.getHeader(SecurityConstants.AUTHORIZATION_HEADER_NAME);
		if (isNull(authorizationHeader) || !authorizationHeader.startsWith(SecurityConstants.TOKEN_PREFIX)) {
			chain.doFilter(request, response);
			return;
		}

		UsernamePasswordAuthenticationToken authenticationToken = getAuthentication(authorizationHeader);
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
		chain.doFilter(request, response);
	}

	UsernamePasswordAuthenticationToken getAuthentication(String authorizationHeader) {
		String token = authorizationHeader.replace(SecurityConstants.TOKEN_PREFIX, "");
		String userName = Jwts.parserBuilder().setSigningKey(SecurityConstants.PRIVATE_KEY).build()
				.parseClaimsJws(token).getBody().getSubject();

		if (nonNull(userName)) {
			var userEntity = userRepository.findByEmail(userName);
			if (isNull(userEntity)) {
				return null;
			}

			var userPrincipal = new UserPrincipal(userEntity);
			return new UsernamePasswordAuthenticationToken(userName, null, userPrincipal.getAuthorities());
		}

		return null;
	}
}
