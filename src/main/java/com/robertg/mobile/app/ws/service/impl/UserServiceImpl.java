package com.robertg.mobile.app.ws.service.impl;

import static com.robertg.mobile.app.ws.utils.TokenUtils.hasTokenExpired;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.robertg.mobile.app.ws.entity.PasswordResetTokenEntity;
import com.robertg.mobile.app.ws.entity.UserEntity;
import com.robertg.mobile.app.ws.exceptions.UserServiceException;
import com.robertg.mobile.app.ws.repository.PasswordResetTokenRepository;
import com.robertg.mobile.app.ws.repository.RoleRepository;
import com.robertg.mobile.app.ws.repository.UserRepository;
import com.robertg.mobile.app.ws.service.EmailService;
import com.robertg.mobile.app.ws.service.UserService;
import com.robertg.mobile.app.ws.shared.dto.AddressDto;
import com.robertg.mobile.app.ws.shared.dto.UserDto;
import com.robertg.mobile.app.ws.ui.model.response.ErrorMessages;
import com.robertg.mobile.app.ws.utils.TokenUtils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	private final RoleRepository roleRepository;

	private final PasswordResetTokenRepository passwordResetTokenRepository;

	private final PasswordEncoder passwordEncoder;

	private final EmailService emailService;

	@Override
	public UserDto createUser(UserDto userDto) {
		var foundUserEntity = userRepository.findByEmail(userDto.getEmail());
		if (nonNull(foundUserEntity)) {
			throw new UserServiceException(ErrorMessages.RECORD_ALREADY_EXISTS);
		}

		for (AddressDto addressDto : userDto.getAddresses()) {
			addressDto.setUserDetails(userDto);
			addressDto.setAddressId(UUID.randomUUID().toString());
		}

		var modelMapper = new ModelMapper();
		var userEntity = modelMapper.map(userDto, UserEntity.class);

		var encryptedPassword = passwordEncoder.encode(userDto.getPassword());
		userEntity.setEncryptedPassword(encryptedPassword);
		userEntity.setUserId(UUID.randomUUID().toString());

		var emailVerificationToken = TokenUtils.generateEmailVerificationToken(userEntity.getUserId());
		userEntity.setEmailVerificationToken(emailVerificationToken);
		userEntity.setEmailVerificationStatus(false);

		// Set roles
		if (nonNull(userDto.getRoles())) {
			var roles = userDto.getRoles().stream().map(roleRepository::findByName).filter(Objects::nonNull)
					.collect(Collectors.toList());
			userEntity.setRoles(roles);
		}

		var savedUserEntity = userRepository.save(userEntity);

		var result = modelMapper.map(savedUserEntity, UserDto.class);

		emailService.verifyEmail(result);

		return result;
	}

	@Override
	public UserDto getUserByEmail(String email) {
		var foundUserEntityByEmail = userRepository.findByEmail(email);
		if (isNull(foundUserEntityByEmail)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		var modelMapper = new ModelMapper();
		return modelMapper.map(foundUserEntityByEmail, UserDto.class);
	}

	@Override
	public UserDto getUserByUserId(String userId) {
		var foundUserEntityByUserId = userRepository.findByUserId(userId);
		if (isNull(foundUserEntityByUserId)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		var modelMapper = new ModelMapper();
		return modelMapper.map(foundUserEntityByUserId, UserDto.class);
	}

	@Override
	public UserDto updateUser(String userId, UserDto userDto) {
		var foundUserEntityByUserId = userRepository.findByUserId(userId);
		if (isNull(foundUserEntityByUserId)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		foundUserEntityByUserId.setFirstName(userDto.getFirstName());
		foundUserEntityByUserId.setLastName(userDto.getLastName());

		var updatedUserEntity = userRepository.save(foundUserEntityByUserId);

		var modelMapper = new ModelMapper();
		return modelMapper.map(updatedUserEntity, UserDto.class);
	}

	@Override
	public void deleteUser(String userId) {
		var foundUserEntityByUserId = userRepository.findByUserId(userId);
		if (isNull(foundUserEntityByUserId)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		userRepository.delete(foundUserEntityByUserId);
	}

	@Override
	public List<UserDto> getUsers(int page, int size) {
		if (page > 0) {
			page = page - 1;
		}

		var pageable = PageRequest.of(page, size);
		var usersPage = userRepository.findAll(pageable);
		var users = usersPage.getContent();
		var result = new ArrayList<UserDto>(users.size());
		for (UserEntity userEntity : users) {
			var modelMapper = new ModelMapper();
			var userDto = modelMapper.map(userEntity, UserDto.class);
			result.add(userDto);
		}

		return result;
	}

	@Override
	public boolean verifyEmailToken(String token) {
		var userEntity = userRepository.findUserByEmailVerificationToken(token);
		if (isNull(userEntity)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		boolean hasTokenExpired = hasTokenExpired(token);
		if (!hasTokenExpired) {
			userEntity.setEmailVerificationToken(null);
			userEntity.setEmailVerificationStatus(true);
			userRepository.save(userEntity);
			return true;
		}

		return false;
	}

	@Override
	public boolean requestPasswordReset(String email) {
		var userEntity = userRepository.findByEmail(email);
		if (isNull(userEntity)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		var token = TokenUtils.generatePasswordResetToken(userEntity.getUserId());

		var passwordResetTokenEntity = new PasswordResetTokenEntity();
		passwordResetTokenEntity.setToken(token);
		passwordResetTokenEntity.setUserDetails(userEntity);
		passwordResetTokenRepository.save(passwordResetTokenEntity);

		return emailService.sendPasswordResetRequest(userEntity.getFirstName(), userEntity.getEmail(), token);
	}

	@Override
	public boolean resetPassword(String token, String password) {
		if (hasTokenExpired(token)) {
			return false;
		}

		var passwordResetTokenEntity = passwordResetTokenRepository.findByToken(token);
		if (isNull(passwordResetTokenEntity)) {
			return false;
		}

		var userEntity = passwordResetTokenEntity.getUserDetails();
		userEntity.setPassword(passwordEncoder.encode(password));
		userRepository.save(userEntity);

		passwordResetTokenRepository.delete(passwordResetTokenEntity);

		return true;
	}
}
