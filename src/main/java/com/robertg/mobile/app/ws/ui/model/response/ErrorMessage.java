package com.robertg.mobile.app.ws.ui.model.response;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ErrorMessage {

	private final LocalDateTime timestamp = LocalDateTime.now();

	private final String message;
}
