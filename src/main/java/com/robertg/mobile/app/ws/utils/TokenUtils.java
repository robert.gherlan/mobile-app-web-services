package com.robertg.mobile.app.ws.utils;

import static org.springframework.util.StringUtils.hasText;

import java.util.Date;

import com.robertg.mobile.app.ws.config.security.SecurityConstants;

import io.jsonwebtoken.Jwts;
import lombok.extern.log4j.Log4j2;

@Log4j2
public final class TokenUtils {

	public static boolean hasTokenExpired(String token) {
		if (!hasText(token)) {
			return true;
		}
		try {
			var claims = Jwts.parserBuilder().setSigningKey(SecurityConstants.PRIVATE_KEY).build().parseClaimsJws(token)
					.getBody();

			var tokenExpirationDate = claims.getExpiration();
			var todayDate = new Date();

			return tokenExpirationDate.before(todayDate);
		} catch (Exception e) {
			log.error("Invalid JWT token {} received.", token, e);
			return true;
		}
	}

	public static String generateEmailVerificationToken(String userId) {
		return Jwts.builder().setSubject(userId)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
				.signWith(SecurityConstants.PRIVATE_KEY).compact();
	}
	
	public static String generatePasswordResetToken(String userId) {
		return Jwts.builder().setSubject(userId)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.PASSWORD_RESET_EXPIRATION_TIME))
				.signWith(SecurityConstants.PRIVATE_KEY).compact();
	}

	private TokenUtils() {
		throw new IllegalStateException();
	}


}
