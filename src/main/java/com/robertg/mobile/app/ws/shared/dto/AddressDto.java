package com.robertg.mobile.app.ws.shared.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AddressDto implements Serializable {

	private static final long serialVersionUID = 6284981711582448502L;

	private long id;
	private String addressId;
	private String city;
	private String country;
	private String streetName;
	private String postalCode;
	private String type;
	private UserDto userDetails;
}
