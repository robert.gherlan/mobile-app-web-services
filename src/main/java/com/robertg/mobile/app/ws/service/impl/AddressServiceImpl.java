package com.robertg.mobile.app.ws.service.impl;

import static java.util.Objects.isNull;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.robertg.mobile.app.ws.entity.AddressEntity;
import com.robertg.mobile.app.ws.exceptions.UserServiceException;
import com.robertg.mobile.app.ws.repository.AddressRepository;
import com.robertg.mobile.app.ws.repository.UserRepository;
import com.robertg.mobile.app.ws.service.AddressService;
import com.robertg.mobile.app.ws.shared.dto.AddressDto;
import com.robertg.mobile.app.ws.ui.model.response.ErrorMessages;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AddressServiceImpl implements AddressService {

	private final AddressRepository addressRepository;

	private final UserRepository userRepository;

	@Override
	public List<AddressDto> getUserAddresses(String userId) {
		var userEntity = userRepository.findByUserId(userId);
		if (isNull(userEntity)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		var addresses = addressRepository.findAllByUserDetails(userEntity);
		var modelMapper = new ModelMapper();
		var result = new ArrayList<AddressDto>();
		for (AddressEntity addressEntity : addresses) {
			var addressDto = modelMapper.map(addressEntity, AddressDto.class);
			result.add(addressDto);
		}

		return result;
	}

	@Override
	public AddressDto getUserAddress(String userId, String addressId) {
		var userEntity = userRepository.findByUserId(userId);
		if (isNull(userEntity)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		var addressEntity = addressRepository.findByAddressIdAndUserDetails(addressId, userEntity);
		if (isNull(addressEntity)) {
			throw new UserServiceException(ErrorMessages.NO_RECORD_FOUND);
		}

		var modelMapper = new ModelMapper();
		return modelMapper.map(addressEntity, AddressDto.class);
	}

}
