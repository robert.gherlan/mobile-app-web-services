package com.robertg.mobile.app.ws.config.security.filters;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.robertg.mobile.app.ws.config.security.SecurityConstants;
import com.robertg.mobile.app.ws.config.security.UserPrincipal;
import com.robertg.mobile.app.ws.service.UserService;
import com.robertg.mobile.app.ws.ui.model.request.UserLoginRequestModel;

import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;

	private final UserService userService;

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			var credentials = new ObjectMapper().readValue(request.getInputStream(), UserLoginRequestModel.class);

			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(credentials.getEmail(), credentials.getPassword()));
		} catch (IOException e) {
			throw new AuthenticationServiceException(e.getMessage());
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		var userName = ((UserPrincipal) authResult.getPrincipal()).getUsername();

		var token = Jwts.builder().setSubject(userName)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
				.signWith(SecurityConstants.PRIVATE_KEY).compact();

		var userDto = userService.getUserByEmail(userName);

		response.addHeader(SecurityConstants.AUTHORIZATION_HEADER_NAME, SecurityConstants.TOKEN_PREFIX + token);
		response.addHeader(SecurityConstants.USER_ID_HEADER_NAME, userDto.getUserId());
	}
}
