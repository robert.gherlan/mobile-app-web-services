package com.robertg.mobile.app.ws.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.robertg.mobile.app.ws.service.AddressService;
import com.robertg.mobile.app.ws.service.UserService;
import com.robertg.mobile.app.ws.shared.dto.UserDto;
import com.robertg.mobile.app.ws.ui.model.request.PasswordResetModel;
import com.robertg.mobile.app.ws.ui.model.request.PasswordResetRequestModel;
import com.robertg.mobile.app.ws.ui.model.request.RequestOperationName;
import com.robertg.mobile.app.ws.ui.model.request.RequestOperationStatus;
import com.robertg.mobile.app.ws.ui.model.request.UserDetailsRequestModel;
import com.robertg.mobile.app.ws.ui.model.response.AddressResponseModel;
import com.robertg.mobile.app.ws.ui.model.response.OperationStatusResponseModel;
import com.robertg.mobile.app.ws.ui.model.response.UserDetailsResponseModel;
import com.robertg.mobile.app.ws.utils.Roles;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

	private final UserService userService;

	private final AddressService addressService;

	@ApiOperation(value = "The Get User Details Web Service Endpoint", notes = "This Web Service Endpoint returns User Details. User public user id in URL path. For example /users/public-user-id")
	@PostAuthorize("hasRole('ADMIN') && returnObject.userId == principal.userId")
	@GetMapping(value = "/{userId}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public UserDetailsResponseModel getUserByUserId(@PathVariable String userId) {
		var userDto = userService.getUserByUserId(userId);

		var modelMapper = new ModelMapper();
		return modelMapper.map(userDto, UserDetailsResponseModel.class);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public UserDetailsResponseModel createUser(@RequestBody UserDetailsRequestModel userDetails) {
		var modelMapper = new ModelMapper();
		var userDto = modelMapper.map(userDetails, UserDto.class);
		var createdUserDto = userService.createUser(userDto);

		return modelMapper.map(createdUserDto, UserDetailsResponseModel.class);
	}

	@PutMapping(path = "/{userId}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public UserDetailsResponseModel updateUser(@PathVariable String userId,
			@RequestBody UserDetailsRequestModel userDetails) {
		var modelMapper = new ModelMapper();
		var userDto = modelMapper.map(userDetails, UserDto.class);
		userDto.setRoles(List.of(Roles.ROLE_USER.getName()));

		var updatedUserDto = userService.updateUser(userId, userDto);
		return modelMapper.map(updatedUserDto, UserDetailsResponseModel.class);
	}

	@PreAuthorize("hasAuthority('DELETE_AUTHORITY')")
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/{userId}")
	public OperationStatusResponseModel deleteUser(@PathVariable String userId) {
		userService.deleteUser(userId);

		var operationStatusResponseModel = new OperationStatusResponseModel();
		operationStatusResponseModel.setOperationName(RequestOperationName.DELETE);
		operationStatusResponseModel.setOperationResult(RequestOperationStatus.SUCCESS);
		return operationStatusResponseModel;
	}

	@ApiImplicitParams({ @ApiImplicitParam(name = "Authorization", value = "Bearer JWT Token", paramType = "header") })
	@GetMapping
	public List<UserDetailsResponseModel> getUsers(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "size", defaultValue = "25") int size) {
		List<UserDto> foundUsers = userService.getUsers(page, size);
		var users = new ArrayList<UserDetailsResponseModel>(foundUsers.size());
		for (UserDto userDto : foundUsers) {
			var modelMapper = new ModelMapper();
			var userDetailsResponseModel = modelMapper.map(userDto, UserDetailsResponseModel.class);
			users.add(userDetailsResponseModel);
		}

		return users;
	}

	@GetMapping(path = "/{userId}/addresses", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public CollectionModel<AddressResponseModel> getUserAddresses(@PathVariable String userId) {
		var addressesDto = addressService.getUserAddresses(userId);
		var listType = new TypeToken<List<AddressResponseModel>>() {
		}.getType();
		var modelMapper = new ModelMapper();
		List<AddressResponseModel> userAddresses = modelMapper.map(addressesDto, listType);

		for (AddressResponseModel addressResponseModel : userAddresses) {
			var selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserController.class)
					.getUserAddress(userId, addressResponseModel.getAddressId())).withSelfRel();
			addressResponseModel.add(selfLink);
		}

		var userLink = WebMvcLinkBuilder.linkTo(UserController.class).slash(userId).withRel("user");
		var selfLink = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).getUserAddresses(userId)).withSelfRel();

		return CollectionModel.of(userAddresses, userLink, selfLink);
	}

	@GetMapping(path = "/{userId}/addresses/{addressId}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public EntityModel<AddressResponseModel> getUserAddress(@PathVariable String userId,
			@PathVariable String addressId) {
		var addressDto = addressService.getUserAddress(userId, addressId);
		var modelMapper = new ModelMapper();
		var addressResponseModel = modelMapper.map(addressDto, AddressResponseModel.class);

		var userLink = WebMvcLinkBuilder.linkTo(UserController.class).slash(userId).withRel("user");
		var userAddressesLink = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).getUserAddresses(userId)).withRel("addresses");
		var selfLink = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(UserController.class).getUserAddress(userId, addressId))
				.withSelfRel();

		return EntityModel.of(addressResponseModel, Arrays.asList(userLink, userAddressesLink, selfLink));
	}

	@GetMapping("/email-verification")
	public OperationStatusResponseModel verifyEmailToken(@RequestParam(value = "token") String token) {
		boolean isVerified = userService.verifyEmailToken(token);

		var operationStatusResponseModel = new OperationStatusResponseModel();
		operationStatusResponseModel.setOperationName(RequestOperationName.VERIFY_EMAIL);
		if (isVerified) {
			operationStatusResponseModel.setOperationResult(RequestOperationStatus.SUCCESS);
		} else {
			operationStatusResponseModel.setOperationResult(RequestOperationStatus.ERROR);
		}

		return operationStatusResponseModel;
	}

	@PostMapping("/password-reset-request")
	public OperationStatusResponseModel passwordResetRequest(
			@RequestBody PasswordResetRequestModel passwordResetRequestModel) {
		boolean operationResult = userService.requestPasswordReset(passwordResetRequestModel.getEmail());
		var operationStatusResponseModel = new OperationStatusResponseModel();
		operationStatusResponseModel.setOperationName(RequestOperationName.REQUEST_RESET_PASSWORD);
		if (operationResult) {
			operationStatusResponseModel.setOperationResult(RequestOperationStatus.SUCCESS);
		} else {
			operationStatusResponseModel.setOperationResult(RequestOperationStatus.ERROR);
		}

		return operationStatusResponseModel;
	}

	@PostMapping("/password-reset")
	public OperationStatusResponseModel passwordReset(@RequestBody PasswordResetModel passwordResetModel) {
		boolean operationResult = userService.resetPassword(passwordResetModel.getToken(),
				passwordResetModel.getPassword());
		var operationStatusResponseModel = new OperationStatusResponseModel();
		operationStatusResponseModel.setOperationName(RequestOperationName.RESET_PASSWORD);
		if (operationResult) {
			operationStatusResponseModel.setOperationResult(RequestOperationStatus.SUCCESS);
		} else {
			operationStatusResponseModel.setOperationResult(RequestOperationStatus.ERROR);
		}

		return operationStatusResponseModel;
	}
}
