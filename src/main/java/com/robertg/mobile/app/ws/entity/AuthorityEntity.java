package com.robertg.mobile.app.ws.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "authorities")
public class AuthorityEntity implements Serializable {

	private static final long serialVersionUID = -2989683365336771508L;

	@Id
	@GeneratedValue
	private long id;

	@Column(unique = true, length = 50)
	private String name;

	@ManyToMany(mappedBy = "authorities")
	private List<RoleEntity> roles;

	public AuthorityEntity(String name) {
		this.name = name;
	}
}
