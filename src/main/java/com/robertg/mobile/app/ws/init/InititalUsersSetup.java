package com.robertg.mobile.app.ws.init;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import java.util.List;
import java.util.UUID;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.robertg.mobile.app.ws.entity.AuthorityEntity;
import com.robertg.mobile.app.ws.entity.RoleEntity;
import com.robertg.mobile.app.ws.entity.UserEntity;
import com.robertg.mobile.app.ws.repository.AuthorityRepository;
import com.robertg.mobile.app.ws.repository.RoleRepository;
import com.robertg.mobile.app.ws.repository.UserRepository;
import com.robertg.mobile.app.ws.utils.Roles;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class InititalUsersSetup {

	private final AuthorityRepository authorityRepository;

	private final RoleRepository roleRepository;

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	@EventListener
	@Transactional
	public void onApplicationEvent(ApplicationReadyEvent event) {
		var readAuthority = createAuthorityEntity("READ_AUTHORITY");
		var writeAuthority = createAuthorityEntity("WRITE_AUTHORITY");
		var deleteAuthority = createAuthorityEntity("DELETE_AUTHORITY");

		createRole(Roles.ROLE_USER.getName(), List.of(readAuthority, writeAuthority));
		var adminRole = createRole(Roles.ROLE_ADMIN.getName(), List.of(readAuthority, writeAuthority, deleteAuthority));

		var admin = userRepository.findByEmail("admin@example.com");
		if (nonNull(admin)) {
			return;
		}

		admin = new UserEntity();
		admin.setEmail("admin@example.com");
		admin.setEmailVerificationStatus(true);
		admin.setEncryptedPassword(passwordEncoder.encode("admin"));
		admin.setUserId(UUID.randomUUID().toString());
		admin.setFirstName("Admin");
		admin.setLastName("Admin");
		admin.setRoles(List.of(adminRole));

		userRepository.save(admin);
	}

	private AuthorityEntity createAuthorityEntity(String name) {
		var authorityEntity = authorityRepository.findByName(name);
		if (isNull(authorityEntity)) {
			authorityEntity = new AuthorityEntity(name);
			authorityEntity = authorityRepository.save(authorityEntity);
		}

		return authorityEntity;
	}

	private RoleEntity createRole(String name, List<AuthorityEntity> authorities) {
		var roleEntity = roleRepository.findByName(name);
		if (isNull(roleEntity)) {
			roleEntity = new RoleEntity(name, authorities);
			roleEntity = roleRepository.save(roleEntity);
		}

		return roleEntity;
	}
}
