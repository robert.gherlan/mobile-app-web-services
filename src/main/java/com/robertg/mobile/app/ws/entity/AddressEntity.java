package com.robertg.mobile.app.ws.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity(name = "addresses")
public class AddressEntity implements Serializable {

	private static final long serialVersionUID = 5359957909803091468L;

	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false, unique = true)
	private String addressId;

	@Column(nullable = false, length = 50)
	private String city;

	@Column(nullable = false, length = 50)
	private String country;

	@Column(nullable = false, length = 100)
	private String streetName;

	@Column(nullable = false, length = 7)
	private String postalCode;

	@Column(nullable = false, length = 10)
	private String type;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserEntity userDetails;
}
