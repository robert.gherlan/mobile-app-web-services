package com.robertg.mobile.app.ws.config.docs;

import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.client.LinkDiscoverers;
import org.springframework.hateoas.mediatype.collectionjson.CollectionJsonLinkDiscoverer;
import org.springframework.plugin.core.SimplePluginRegistry;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket docket() {
		return new Docket(DocumentationType.SWAGGER_2).protocols(Set.of("HTTP", "HTTPS")).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("com.robertg.mobile.app.ws")).paths(PathSelectors.any())
				.build();
	}

	@Bean
	public LinkDiscoverers discovers() {
		return new LinkDiscoverers(SimplePluginRegistry.of(new CollectionJsonLinkDiscoverer()));
	}

	protected ApiInfo apiInfo() {
		var contact = new Contact("Robert Gherlan", "http://www.mobileappws.com", "robertgherlan@gmail.com");
		return new ApiInfoBuilder().title("Phot Application RESTful Web Service Documentation")
				.description("This pages documents Photo App RESTful Web Service endpoints.").version("1.0")
				.termsOfServiceUrl("http://www.mobileappws.com/service.html").contact(contact).license("Apache 2.0")
				.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html").build();
	}
}

@Configuration
class SwaggerUiWebMvcConfigurer implements WebMvcConfigurer {

	private final String baseUrl;

	public SwaggerUiWebMvcConfigurer(@Value("${springfox.documentation.swagger-ui.base-url:}") String baseUrl) {
		this.baseUrl = baseUrl;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		var baseUrlTrimTrailedSlash = StringUtils.trimTrailingCharacter(this.baseUrl, '/');
		registry.addResourceHandler(baseUrlTrimTrailedSlash + "/swagger-ui/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/")
				.resourceChain(false);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController(baseUrl + "/swagger-ui/")
				.setViewName("forward:" + baseUrl + "/swagger-ui/index.html");
	}
}
