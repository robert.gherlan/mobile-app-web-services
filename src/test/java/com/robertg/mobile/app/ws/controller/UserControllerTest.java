package com.robertg.mobile.app.ws.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.robertg.mobile.app.ws.service.UserService;
import com.robertg.mobile.app.ws.shared.dto.UserDto;
import com.robertg.mobile.app.ws.ui.model.response.UserDetailsResponseModel;

class UserControllerTest {

	@Mock
	private UserService userService;

	@InjectMocks
	private UserController userController;

	private UserDto userDto;

	private final String userId = "USER_ID";

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);

		userDto = new UserDto();
		userDto.setUserId(userId);
		userDto.setFirstName("Robert");
		userDto.setLastName("G");
		userDto.setEmail("robertg@example.com");

	}

	@Test
	void testGetUserByUserIdString() {
		when(userService.getUserByUserId(userId)).thenReturn(userDto);
		UserDetailsResponseModel userDetailsResponseModel = userController.getUserByUserId(userId);
		assertThat(userDetailsResponseModel).isNotNull();
		assertThat(userDetailsResponseModel.getFirstName()).isEqualTo(userDto.getFirstName());
		assertThat(userDetailsResponseModel.getLastName()).isEqualTo(userDto.getLastName());
		assertThat(userDetailsResponseModel.getEmail()).isEqualTo(userDto.getEmail());
		assertThat(userDetailsResponseModel.getUserId()).isEqualTo(userDto.getUserId());
		verify(userService).getUserByUserId(userId);
	}
}
