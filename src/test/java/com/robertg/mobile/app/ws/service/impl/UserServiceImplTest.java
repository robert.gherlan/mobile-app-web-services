package com.robertg.mobile.app.ws.service.impl;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;

import com.robertg.mobile.app.ws.entity.UserEntity;
import com.robertg.mobile.app.ws.exceptions.UserServiceException;
import com.robertg.mobile.app.ws.repository.UserRepository;
import com.robertg.mobile.app.ws.service.EmailService;
import com.robertg.mobile.app.ws.shared.dto.AddressDto;
import com.robertg.mobile.app.ws.shared.dto.UserDto;

class UserServiceImplTest {

	@InjectMocks
	private UserServiceImpl userServiceImpl;

	@Mock
	private UserRepository userRepository;

	@Mock
	private PasswordEncoder passwordEncoder;
	
	@Mock
	private EmailService emailService;

	private UserEntity userEntity;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		doNothing().when(emailService).verifyEmail(any());

		userEntity = new UserEntity();
		userEntity.setId(1);
		userEntity.setEmail("robertg@example.com");
		userEntity.setFirstName("Robert");
		userEntity.setLastName("G");
		userEntity.setUserId("123abc");
		userEntity.setPassword("password");
		userEntity.setEncryptedPassword("ashfgahfbvactr123");
	}

	@Test
	void testGetUserByEmail() {
		when(userRepository.findByEmail(anyString())).thenReturn(userEntity);

		UserDto foundUserDto = userServiceImpl.getUserByEmail("robertg@example.com");
		assertThat(foundUserDto).isNotNull();
		assertThat(foundUserDto.getFirstName()).isEqualTo(userEntity.getFirstName());
	}

	@Test
	void testGetUserByEmail_userNotFound() {
		when(userRepository.findByEmail(anyString())).thenReturn(null);
		assertThatThrownBy(() -> userServiceImpl.getUserByEmail("robertg@example.com"))
				.isInstanceOf(UserServiceException.class);
	}

	@Test
	void testCreateUser_userAlreadyExists() {
		when(userRepository.findByEmail(anyString())).thenReturn(userEntity);
		UserDto userDto = new UserDto();
		userDto.setEmail("robertg@example.com");
		assertThatThrownBy(() -> userServiceImpl.createUser(userDto)).isInstanceOf(UserServiceException.class);
	}

	@Test
	void testCreateUser() {
		when(userRepository.findByEmail(anyString())).thenReturn(null);
		when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");
		when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);

		AddressDto shippingAddressDto = new AddressDto();
		shippingAddressDto.setType("shipping");
		shippingAddressDto.setAddressId("shippingAddressId");
		shippingAddressDto.setCity("Vancouver123");
		shippingAddressDto.setCountry("Canada");
		shippingAddressDto.setStreetName("ABCDE Street Name");
		shippingAddressDto.setPostalCode("1234FDS");
		shippingAddressDto.setAddressId("123VFD");

		AddressDto billingAddressDto = new AddressDto();
		billingAddressDto.setType("billing");
		billingAddressDto.setAddressId("billingAddressId");
		billingAddressDto.setCity("Vancouver");
		billingAddressDto.setCountry("Canada");
		billingAddressDto.setStreetName("ABC Street Name");
		billingAddressDto.setPostalCode("1234AVB");
		billingAddressDto.setAddressId("123ABC");

		UserDto userDto = new UserDto();
		userDto.setFirstName("Robert");
		userDto.setLastName("Gherlan");
		userDto.setPassword("password");
		userDto.setEmail("robertg@example.com");
		userDto.setAddresses(List.of(shippingAddressDto, billingAddressDto));

		UserDto storedUserDto = userServiceImpl.createUser(userDto);
		assertThat(storedUserDto).isNotNull();
		assertThat(storedUserDto.getFirstName()).isEqualTo(userEntity.getFirstName());
		assertThat(storedUserDto.getLastName()).isEqualTo(userEntity.getLastName());
		assertThat(storedUserDto.getUserId()).isNotNull();

		verify(userRepository).save(any());
		verify(userRepository).findByEmail(anyString());
		verify(passwordEncoder).encode("password");
	}
}
