package com.robertg.mobile.app.ws.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.robertg.mobile.app.ws.entity.UserEntity;

@DataJpaTest
class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;

	@BeforeEach
	void setUp() {
		UserEntity userEntity = new UserEntity();
		userEntity.setEmail("userEmail@example.com");
		userEntity.setFirstName("UserFirstName");
		userEntity.setLastName("UserLastName");
		userEntity.setUserId("123abc");
		userEntity.setPassword("password");
		userEntity.setEncryptedPassword("encryptedPassword");
		userEntity.setEmailVerificationStatus(true);

		userRepository.save(userEntity);

		UserEntity userEntity1 = new UserEntity();
		userEntity1.setEmail("userEmail1@example.com");
		userEntity1.setFirstName("UserFirstName");
		userEntity1.setLastName("UserLastName");
		userEntity1.setUserId("123abcd");
		userEntity1.setPassword("password1");
		userEntity1.setEncryptedPassword("encryptedPassword1");
		userEntity1.setEmailVerificationStatus(true);

		userRepository.save(userEntity1);
	}

	@Test
	void testFindAllUsersWithConfirmedEmailAddress() {
		Pageable pageable = PageRequest.of(0, 2);
		Page<UserEntity> pages = userRepository.findAllUsersWithConfirmedEmailAddress(pageable);
		assertThat(pages).isNotNull();
		assertThat(pages.getContent()).hasSize(2);
	}

	@Test
	void testFindUsersByFirstName() {
		List<UserEntity> users = userRepository.findUsersByFirstName("UserFirstName");
		assertThat(users).hasSize(2);
	}

	@Test
	void testFindUsersByLastName() {
		List<UserEntity> users = userRepository.findUsersByLastName("UserLastName");
		assertThat(users).hasSize(2);
	}

	@Test
	void testFindUsersByKeyword() {
		var keyword = "LastN";
		List<UserEntity> users = userRepository.findUsersByKeyword(keyword);
		assertThat(users).hasSize(2);
		for (UserEntity user : users) {
			assertThat(user.getLastName()).contains(keyword);
		}
	}

	@Test
	void testFindUserFirstNameAndLastNameByKeyword() {
		var keyword = "LastN";
		List<Object[]> users = userRepository.findUserFirstNameAndLastNameByKeyword(keyword);
		assertThat(users).hasSize(2);
		for (Object[] user : users) {
			assertThat(user[0]).isNotNull();
			assertThat(user[1]).isNotNull();
		}
	}

	@Test
	void testGetUserEntityFullNameByUserId() {
		String userId = "123abc";
		List<Object[]> users = userRepository.getUserEntityFullNameByUserId(userId);
		assertThat(users).hasSize(1);
		assertThat(users.get(0)[0]).isNotNull();
		assertThat(users.get(0)[1]).isNotNull();
	}

	@Test
	void testFindUserEntityByUserId() {
		String userId = "123abc";
		UserEntity userEntity = userRepository.findUserEntityByUserId(userId);
		assertThat(userEntity.getUserId()).isEqualTo(userId);
	}

	@Test
	void testUpdateUserEmailVerificationStatus() {
		String userId = "123abc";
		UserEntity userEntity = userRepository.findByUserId(userId);
		assertThat(userEntity.isEmailVerificationStatus()).isTrue();

		userRepository.updateUserEmailVerificationStatus(false, userId);

		UserEntity updatedUserEntity = userRepository.findByUserId(userId);
		assertThat(updatedUserEntity.isEmailVerificationStatus()).isFalse();
	}

	@Test
	void testUpdateUserEntityEmailVerificationStatus() {
		String userId = "123abc";
		UserEntity userEntity = userRepository.findByUserId(userId);
		assertThat(userEntity.isEmailVerificationStatus()).isTrue();

		userRepository.updateUserEntityEmailVerificationStatus(false, userId);

		UserEntity updatedUserEntity = userRepository.findByUserId(userId);
		assertThat(updatedUserEntity.isEmailVerificationStatus()).isFalse();
	}

}
